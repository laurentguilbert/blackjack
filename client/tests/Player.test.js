import React from 'react'
import { mount } from 'enzyme'

import Hand from '../src/components/Hand'
import Player from '../src/components/Player'
import { STATUS_DISPLAY } from '../src/components/Player'


import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
Enzyme.configure({ adapter: new Adapter() })


describe('Player', () => {
  let props
  let mountedPlayer
  const player = () => {
    if (mountedPlayer === undefined) {
      mountedPlayer = mount(
        <Player {...props} />
      )
    }
    return mountedPlayer
  }

  beforeEach(() => {
    props = {
      hand: [],
      status: undefined,
      name: undefined
    }
    mountedPlayer = undefined
  })

  it('always renders a div', () => {
    expect(player().find('.player').length).toBe(1);
  });

  describe('when `hand` is empty', () => {

    it('does not render `Hand`', () => {
      expect(player().find(Hand).length).toBe(0)
    })
  })

  describe('when `hand` is not empty', () => {

    beforeEach(() => {
      props.hand = [1, 2, 3]
    })

    it('renders `Hand`', () => {
      expect(player().find(Hand).length).toBe(1)
    })

    it("sets the rendered `Hand`'s' `cards` prop to the same value as `hand`", () => {
      const hand = player().find(Hand)
      expect(hand.props().cards).toBe(props.hand)
    })
  })

  describe('when `status` is undefined', () => {

    it('does not add `data-status` prop to the root div', () => {
      expect(player().find('.player').props()['data-status']).toBe(undefined)
    })
  })

  describe('when `name` is passed', () => {

    beforeEach(() => {
      props.name = 'Foobar'
    })

    it('renders `player-infos` div', () => {
      expect(player().find('.player-infos').length).toBe(1)
    })

    it('renders `player-name` div with the same value as `name`', () => {
      expect(player().find('.player-name').text()).toBe(props.name)
    })

    it('does not render `player-status` div', () => {
      expect(player().find('.player-status').length).toBe(0)
    })
  })

  describe('when `status` is passed', () => {

    beforeEach(() => {
      props.status = 1
    })

    it('adds `data-status` prop to the root div', () => {
      expect(player().find('.player').props()['data-status']).toBe(props.status)
    })

    it('renders `player-infos` div', () => {
      expect(player().find('.player-infos').length).toBe(1)
    })

    // it('renders `player-status` div with the display value for `status`', () => {
    //   expect(player().find('.player-status').text()).toBe(STATUS_DISPLAY[props.status])
    // })

    it('does not render `player-name` div', () => {
      expect(player().find('.player-name').length).toBe(0)
    })
  })
})
