import PropTypes from 'prop-types'
import React from 'react'

import Hand from './Hand'


export const STATUS_DISPLAY = {
  0: 'Playing',
  1: 'Standing',
  2: 'Waiting',
  3: 'Lost',
  4: 'Win'
}


export default class Player extends React.Component {

  render() {
    const { hand, handValue, status, name, chips } = this.props

    let handNode
    if (hand !== undefined && hand.length > 0) {
      handNode = <Hand cards={hand} handValue={handValue} />
    }

    let playerProps = {}
    if (status !== undefined) {
      playerProps['data-status'] = status
    }

    return (
      <div className="player" {...playerProps}>
        {handNode}
        <div className="player-infos">
          {name !== undefined ?
            <div className="player-name">{name}</div>
          : null}
          {chips !== undefined ?
            <div className="player-chips">${chips}</div>
          : null}
          {status !== undefined ?
            <div className="player-status">{STATUS_DISPLAY[status]}</div>
          : null}
        </div>
      </div>
    )
  }
}

Player.propTypes = {
  hand: PropTypes.array,
  name: PropTypes.string,
  status: PropTypes.number,
  chips: PropTypes.number
}
