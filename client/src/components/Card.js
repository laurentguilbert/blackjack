import PropTypes from 'prop-types'
import React from 'react'


export default class Card extends React.Component {

  getRank() {
    const rank = this.props.index % 13
    switch (rank) {
      case 1:
        return 'A'
      case 11:
        return 'J'
      case 12:
        return 'Q'
      case 0:
        return 'K'
    }
    return rank
  }

  getSuit() {
    const suit = Math.ceil(this.props.index / 13)
    switch (suit) {
      case 1:
        return 'S'
      case 2:
        return 'H'
      case 3:
        return 'D'
      case 4:
        return 'C'
    }
  }

  render() {
    const rank = this.getRank()
    const suit = this.getSuit()
    return (
      <div className={"card suit-" + suit + " rank-" + rank}>
        <div className="card-rank">{rank}</div>
      </div>
    )
  }
}

Card.propTypes = {
  index: PropTypes.number.isRequired
}
