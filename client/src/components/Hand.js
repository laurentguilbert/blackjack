import PropTypes from 'prop-types'
import React from 'react'

import Card from './Card'


export default class Hand extends React.Component {

  render() {
    const { cards, handValue } = this.props
    return (
      <div className="hand">
        {cards.map((cardIndex, index) =>
          <Card index={cardIndex} key={index} />
        )}
        {handValue !== undefined ?
          <div className="hand-value">{handValue}</div>
        : null}
      </div>
    )
  }
}

Hand.propTypes = {
  cards: PropTypes.array,
  handValue: PropTypes.number
}
