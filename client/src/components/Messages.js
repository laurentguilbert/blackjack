import PropTypes from 'prop-types'
import React from 'react'


export default class Messages extends React.Component {

  render() {
    const { messages } = this.props
    return (
      <div className="messages">
        {messages.map((message, index) => (
          <div className="message" data-level={message.level} key={index}>
            <span className="message-time">{message.time}</span>
            <span className="message-content">{message.content}</span>
          </div>
        ))}
      </div>
    )
  }
}

Messages.propTypes = {
  messages: PropTypes.array
}
