import React from 'react'

import Messages from './Messages'
import Player from './Player'
import socket from '../socket'


export default class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      dealer: {},
      players: {},
      playersOrder: [],
      messages: []
    }
  }

  hit() {
    socket.emit('hit')
  }

  stand() {
    socket.emit('stand')
  }

  componentDidMount() {
    socket.on('newGame', state => {
      this.setState(state)
      console.log('newGame', state)
    })

    socket.on('gameState', state => {
      this.setState(state)
      console.log('gameState', state)
    })

    socket.on('updatePlayer', (id, player) => {
      const players = Object.assign({}, this.state.players)
      players[id] = player
      this.setState({ players })
      console.log('updatePlayer', id, player)
    })

    socket.on('updateDealer', dealer => {
      this.setState({ dealer })
      console.log('updateDealer', dealer)
    })

    socket.on('removePlayer', id => {
      if (this.state.playersOrder.indexOf(id) !== -1) {
        const playersOrder = this.state.playersOrder.slice()
        if (playersOrder.indexOf(id) !== -1)
        playersOrder.splice(playersOrder.indexOf(id), 1)
        this.setState({ playersOrder })
      }
      console.log('removePlayer', id)
    })

    socket.on('message', (level, time, content) => {
      const messages = this.state.messages.slice(
        -Math.min(this.state.messages.length, 14)
      )
      messages.push({ time, level, content })
      this.setState({ messages })
      console.log('message', level, time, content)
    })
  }

  render() {
    let actionNodes
    let currentPlayerNode
    if (
      socket.id !== undefined &&
      this.state.players[socket.id] !== undefined
    ) {
      const currentPlayer = this.state.players[socket.id]
      currentPlayerNode = <Player {...currentPlayer} />
      actionNodes = (
        <div className={"actions" + (currentPlayer.status !== 0 ? " is-disabled" : "")}>
          <div className="action hit" onClick={this.hit}>HIT</div>
          <div className="action stand" onClick={this.stand}>STAND</div>
        </div>
      )
    }
    return (
      <div className="table">
        <Messages messages={this.state.messages} />
        <div className="dealer">
          <Player {...this.state.dealer} />
        </div>
        <div className="other-players">
          {this.state.playersOrder.map(id =>
            <Player key={id} {...this.state.players[id]} />
          )}
        </div>
        <div className="current-player">
          {currentPlayerNode}
          {actionNodes}
        </div>
      </div>
    )
  }
}
