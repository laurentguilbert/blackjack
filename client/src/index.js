import React from 'react'
import { render } from 'react-dom'

import App from './components/App'

import './assets/favicon.png'


render(<App />, document.getElementById('root'))
