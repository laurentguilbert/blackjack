const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');


module.exports = {
  entry: [
    './src/theme/style.less',
    './src/index.js'
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.less$/,
        loader: 'style-loader!css-loader!less-loader'
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        loader: 'file-loader?name=[name].[ext]'
      }
    ]
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'client.js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.tpl.html',
      filename: 'index.html',
      inject: 'body'
    })
  ]
};
