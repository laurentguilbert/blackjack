var nodeExternals = require('webpack-node-externals');
var NodemonPlugin = require('nodemon-webpack-plugin');
var path = require('path');


module.exports = {
  target: 'node',
  entry: './src/index.js',
  externals: [
    nodeExternals()
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      }
    ]
  },
  output: {
    path: path.resolve(__dirname, 'bin'),
    filename: 'server.js'
  },
  plugins: [
    new NodemonPlugin()
  ]
};
