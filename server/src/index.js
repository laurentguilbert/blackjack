import 'colors'
import express from 'express'
import http from 'http'
import path from 'path'
import socketIO from 'socket.io'

import Game from './Game'


const app = express()
const server = http.Server(app)
const io = new socketIO(server)

const port = process.env.PORT || 3000
const clientDist = path.resolve('..', 'client', 'dist')

const game = new Game(io)


app.use(express.static(clientDist))

app.get('/', (req, res) => {
  res.sendFile(path.join(clientDist, 'index.html'))
})

io.on('connection', (socket) => {
  game.addPlayer(socket)

  socket.on('disconnect', () => {
    game.removePlayer(socket.client.id)
  })
})

server.listen(port, () => {
  console.log('Listening'.green.bold, port)
})
