const STATUS_PLAYING = 0
const STATUS_STANDING = 1
const STATUS_WAITING = 2
const STATUS_LOST = 3
const STATUS_WIN = 4

const MESSAGE_LEVEL_INFO = 0
const MESSAGE_LEVEL_IMPORTANT = 1

const PLAYER_NAMES = [
  'Covfefe',
  'Bukafé',
  'Crossfist',
  'Spiki',
  'TQDM',
  'Papa Shultz',
  'Christofouille',
  'Bob'
]

const STARTING_CHIPS = 1000
const ROUND_DURATION = 15000
const TIME_BEFORE_RESTART = 5000


function pad(n, width) {
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n
}

function getTimeLeft(timeout) {
  return Math.ceil(
    (timeout._idleStart + timeout._idleTimeout - Date.now()) / 1000
  )
}


export default class Game {

  constructor(io) {
    this.io = io

    this.sockets = {}
    this.players = {}
    this.playersOrder = []

    this.dealer = {
      hand: []
    }

    this.playersWaitingCount = 0
  }

  getPlayerName() {
    return PLAYER_NAMES[Math.floor(Math.random() * PLAYER_NAMES.length)]
  }

  getCardValue(card) {
    const rank = card % 13
    if (rank === 11 || rank === 12 || rank === 0) {
      return 10
    } else if (rank === 1) {
      return 11
    }
    return rank
  }

  getHandValue(player) {
    if (player.hand === undefined) {
      return 0
    }
    let handValues = player.hand.map(card => this.getCardValue(card))
    while (
      handValues.reduce((sum, value) => sum + value, 0) > 21 &&
      handValues.indexOf(11) !== -1
    ) {
      handValues[handValues.indexOf(11)] = 1
    }
    return handValues.reduce((sum, value) => sum + value, 0)
  }

  drawCards(player, count) {
    if (count === undefined) {
      count = 1
    }
    for (let index = 0; index < count; index++) {
      const newCard = Math.floor(Math.random() * 52) + 1
      player.hand.push(newCard)
    }
    player.handValue = this.getHandValue(player)
  }

  playerLost(player) {
    console.log(player.id, 'LOST'.yellow)
    player.status = STATUS_LOST
    player.chips -= 50
    this.io.emit('updatePlayer', player.id, player)
    this.sendMessage(
      MESSAGE_LEVEL_INFO,
      `${player.name} lost with ${player.handValue}.`
    )
  }

  playerWin(player) {
    console.log(player.id, 'WIN'.yellow)
    player.status = STATUS_WIN
    player.chips += 100
    this.io.emit('updatePlayer', player.id, player)
    this.sendMessage(
      MESSAGE_LEVEL_INFO,
      `${player.name} won with ${player.handValue}.`
    )
  }

  reset() {
    this.playersOrder.forEach(id => {
      const player = this.players[id]
      player.status = STATUS_PLAYING
      player.hand = []
    })
    this.dealer.hand = []
    this.playersWaitingCount = 0
  }

  timeReminder() {
    const remaining = (
      Math.round((ROUND_DURATION - (new Date() - this.roundStartTime)) / 1000)
    )
    if (remaining <= 5) {
      this.sendMessage(
        MESSAGE_LEVEL_IMPORTANT,
        `${remaining} seconds remaining.`
      )
    }
  }

  start() {
    console.log('START GAME'.blue.bold)
    this.reset()
    this.drawCards(this.dealer)
    this.playersOrder.forEach(id => {
      this.drawCards(this.players[id], 2)
    })
    this.io.emit('newGame', {
      dealer: this.dealer,
      players: this.players,
      playersOrder: this.playersOrder
    })

    this.sendMessage(MESSAGE_LEVEL_IMPORTANT, `New round starting.`)
    this.roundStartTime = new Date()
    this.roundTimeout = setTimeout(() => this.finish(), ROUND_DURATION)
    this.remainingTimeInterval = setInterval(() => this.timeReminder(), 1000)
  }

  finish() {
    console.log('FINISH GAME'.blue)
    clearTimeout(this.restartTimeout)
    clearTimeout(this.roundTimeout)
    clearInterval(this.remainingTimeInterval)

    this.playDealer()
    const dealer = this.dealer
    this.playersOrder.forEach(id => {
      const player = this.players[id]
      if (
        player.status === STATUS_LOST || 
        player.status === STATUS_WAITING
      ) {
        return
      }
      const dealerHandValue = this.getHandValue(dealer)
      if (
        dealerHandValue > 21 ||
        this.getHandValue(player) > dealerHandValue
      ) {
        this.playerWin(player)
      } else {
        this.playerLost(player)
      }
    })

    if (this.playersOrder.length > 0) {
      this.sendMessage(
        MESSAGE_LEVEL_IMPORTANT,
        `Round finished, restarting in 5 seconds...`
      )
      this.restartTimeout = setTimeout(() => this.start(), TIME_BEFORE_RESTART)
    }
  }

  sendMessage(level, content, playerId) {
    const now = new Date()
    const hours = pad(now.getHours(), 2)
    const minutes = pad(now.getMinutes(), 2)
    const seconds = pad(now.getSeconds(), 2)
    const timeDisplay = `[${hours}:${minutes}:${seconds}]`
    if (playerId !== undefined && this.sockets[playerId] !== undefined) {
      this.sockets[playerId].emit(level, timeDisplay, content)
    } else {
      this.io.emit('message', level, timeDisplay, content)
    }
  }

  playDealer() {
    let playersLostCount = 0
    this.playersOrder.forEach(id => {
      if (this.players[id].status === STATUS_LOST) {
        playersLostCount++
      }
    })
    if (playersLostCount === this.playersOrder.length) {
      // Nothing to do since everyone already lost.
      return
    }
    const dealer = this.dealer
    while (this.getHandValue(dealer) < 17) {
      this.drawCards(dealer)
    }
    this.io.emit('updateDealer', dealer)
  }

  checkGameStatus() {
    let playersDoneCount = 0
    this.playersOrder.forEach(id => {
      if (this.players[id].status !== STATUS_PLAYING) {
        playersDoneCount++
      }
    })
    if (playersDoneCount >= this.playersOrder.length) {
      this.finish()
    }
  }

  stand(id) {
    console.log(id, 'STAND'.yellow)
    const player = this.players[id]
    if (player.status === STATUS_PLAYING) {
        player.status = STATUS_STANDING
        this.io.emit('updatePlayer', id, player)
        this.checkGameStatus()
    }
  }

  hit(id) {
    console.log(id, 'HIT'.yellow)
    const player = this.players[id]
    if (player.status === STATUS_PLAYING) {
      this.drawCards(player)
      if (this.getHandValue(player) > 21) {
        this.playerLost(player)
        this.checkGameStatus()
      } else {
        this.io.emit('updatePlayer', id, player)
      }
    }
  }

  addPlayer(socket) {
    const socketId = socket.client.id
    console.log('NEW PLAYER'.green, socketId)

    const name = this.getPlayerName()
    this.players[socketId] = {
      id: socketId,
      name: name,
      status: STATUS_WAITING,
      chips: STARTING_CHIPS
    }
    this.playersOrder.push(socketId)
    this.playersWaitingCount++

    // Bind socket messages.
    socket.on('hit', () => this.hit(socketId))
    socket.on('stand', () => this.stand(socketId))

    this.sendMessage(MESSAGE_LEVEL_INFO, `Welcome ${name}!`)

    if (this.playersOrder.length === this.playersWaitingCount) {
      this.start()
    } else {
      socket.emit('gameState', {
        dealer: this.dealer,
        players: this.players,
        playersOrder: this.playersOrder
      })
    }
  }

  removePlayer(socketId) {
    console.log('PLAYER LEFT'.red, socketId)
    this.sendMessage(MESSAGE_LEVEL_INFO, `${this.players[socketId].name} left.`)
    delete this.players[socketId]
    delete this.sockets[socketId]
    this.playersOrder.splice(this.playersOrder.indexOf(socketId), 1)
    this.checkGameStatus()
    this.io.emit('removePlayer', socketId)
  }
}
